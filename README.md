**geo-django-project**

Sample Screenview
![geo-django-weather-info](https://gitlab.com/pradeeprajputpsd/geo-django-project/-/raw/master/doc/geo-django-homepage.png)


Tech-Stack used for it
1. Django
2. GeoDjango
3. Django-leaflet
4. PostGIS - for geo database
 



Third party Api is used to get tempearture and humidity value of a particulat lat long coordinates:  

        api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={your api key}

Please use this url to generate api key:
https://openweathermap.org/api

Initial setup to run it in your local system(ubuntu):

1. create a virtual environment:

        sudo apt-get install virtualenv
        virtualenv venv
        source venv/bin/activate

2. git clone
3. cd geo-django-project

4.  Install pip packages:
        
        pip install -r requirements.txt   

5. Installing GeoDjango Dependencies (GEOS, GDAL, and PROJ.4)
        
        sudo aptitude install gdal-bin libgdal-dev
        sudo aptitude install python3-gdal
        sudo aptitude install binutils libproj-dev

Setting up a Spatial Database With PostgreSQL and PostGIS:
    
    docker run --name=postgis -d -e POSTGRES_USER=user001 -e POSTGRES_PASS=123456789 -e POSTGRES_DBNAME=gis -p 5432:5432 kartoza/postgis:9.6-2.4



Run the the django app:

        python manage.py makemigrations
        python manage.py migrate
        python manage.py runserver


Test the code:   

    Python manage.py test                 -----> it would be test functionality                                                                       
    coverage run manage.py test -v 2      -----> it would be test code coverage




Reference:

1) https://docs.djangoproject.com/en/3.0/ref/contrib/gis/tutorial/ 
2) https://django-leaflet.readthedocs.io/en/latest/ 
3) https://docs.djangoproject.com/en/3.0/topics/testing/advanced/#integration-with-coverage-py  
