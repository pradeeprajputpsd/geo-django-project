from django.test import TestCase, Client
from .models import WeatherLatLong
from django.utils import timezone
# Create your tests here.
from django.urls import reverse
from django.contrib.gis.geos import Polygon, Point


class WeatherLatLongTest(TestCase):

    def create_weatherlatlon_entry(self, name="assam", temperature=300,humidity=90,latitude =26.2006,longitude=92.9376 ):
        return WeatherLatLong.objects.create(name=name, temperature=temperature, humidity=humidity,location =Point(longitude,latitude))
    # models test
    def test_weatherlatlon_entry_creation(self):
        w = self.create_weatherlatlon_entry()
        self.assertTrue(isinstance(w, WeatherLatLong))
        self.assertEqual(w.__unicode__(), w.name)
    #url_test    
    def index_page(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200) 
        
    def update_data_on_map(self):
        response = self.client.get('/update-data-on-map')
        self.assertEqual(response.status_code, 200)    
        
    def test_case_failure(self):
        self.assertEqual(1,2)
     
 