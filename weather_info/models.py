from django.db import models as normal_models
# Create your models here.
from django.contrib.gis.db import models


class WeatherLatLong(models.Model):
    name = models.CharField(max_length=100)
    location = models.PointField()
    temperature = models.FloatField()
    humidity = models.FloatField()
    
    def __str__(self):
        return self.name
    
    def __unicode__(self):
        return self.name
    
    
class WeatherData(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=100)
    state = models.CharField(max_length=100)
    country = models.CharField(max_length=100)
    latitude = models.FloatField()
    longitude = models.FloatField()
    point = models.PointField()
    