from django.contrib import admin
#from leaflet.admin import LeafletGeoAdmin
# Register your models here.
from leaflet.admin import LeafletGeoAdmin
from .models import WeatherLatLong, WeatherData

admin.site.register(WeatherLatLong, LeafletGeoAdmin)
admin.site.register(WeatherData, LeafletGeoAdmin)