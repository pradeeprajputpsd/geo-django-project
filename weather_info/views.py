from django.shortcuts import render,HttpResponse,HttpResponseRedirect
import datetime
import requests
import json
from django.contrib.gis.geos import Polygon, Point
from django.core.serializers import serialize
from django.views.generic.base import TemplateView
from django.conf import settings as conf_settings
# Create your views here.
from .models import WeatherLatLong,WeatherData

OPEN_WEATHER_MAP_API_KEY = conf_settings.OPEN_WEATHER_MAP_API_KEY


def current_datetime(request):
    now = datetime.datetime.now()
    html = "<html><body>It is now %s.</body></html>" % now
    return HttpResponse(html)
 

class WeatherApi(TemplateView):
    template_name = 'home/index.html'

    def get_context_data(self, *args, **kwargs):
        context = super(WeatherApi, self).get_context_data(*args, **kwargs)
        context['message'] = ''
        return context


def dataset(request):
    weather = serialize('geojson', WeatherLatLong.objects.all())
    return HttpResponse(weather)


def weather_data(request):
    with open('static/city.list.json') as f:
        data = json.load(f)
        for row in data:
            id = row.get('id')
            name = row.get('name')
            state = row.get('state')
            country =  row.get('country')
            lat =  row['coord'].get('lat')
            lon =  row['coord'].get('lon')
            point = Point(lon, lat,srid=4326)
            WeatherData(id=id,name=name,state=state,country=country,latitude=lat, longitude=lon,point=point).save()
    return HttpResponse('successfully saved')

def add_data_on_map(request):
    message = ''
    if 'add' in request.POST:
        city = request.POST.get('city')
        data_out = WeatherLatLong.objects.filter(name__icontains=city).first()
        if data_out:
            message = "This place is already pointing on map"
        else:
            data = WeatherData.objects.filter(name__icontains=city).first()
            if data:
                latitude = data.latitude
                longitude = data.longitude
                point = Point(longitude, latitude)
                url = "http://api.openweathermap.org/data/2.5/weather?lat={}&lon={}&appid={}".format(latitude,longitude,OPEN_WEATHER_MAP_API_KEY)
                url_data =  requests.get(url)
                if url_data.status_code == 200:
                    temperature = url_data.json()['main'].get('temp')
                    humidity = url_data.json()['main'].get('humidity')
                    WeatherLatLong(name=city,location=point,temperature=temperature,humidity=humidity).save()
                else:
                    message = "openweathermap api response is ".format(url_data.status_code)
            else:
                message = "This place is not available on our database"
    elif 'delete' in request.POST:
        city = request.POST.get('city')
        data_out = WeatherLatLong.objects.filter(name__icontains=city).first()
        if data_out:
            id = data_out.id
            WeatherLatLong.objects.get(id=id).delete()
            message = '{}  mark has been removed from map'.format(city)
        else:
            message = 'Please provide valid place name to delete mark from map'
        
    return render(request,'home/index.html',{'message':message})
        

