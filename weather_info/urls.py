from . import views
from django.conf.urls import  url
from django.urls import include, path
from django.views.generic import TemplateView
from djgeojson.views import GeoJSONLayerView
from .models import WeatherData

app_name = "weather_info"

urlpatterns = [
    path('date/', views.current_datetime, name="current_datetime"), #to add home page urls
    path('', views.WeatherApi.as_view(), name="weather_api"),
    path('weather-data', views.weather_data, name="weather_data"),
    path('update-data-on-map', views.add_data_on_map, name="update_data_on_map"),
    path('data/', views.dataset, name='data')
]